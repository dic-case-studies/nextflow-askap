# Nextflow ASKAP

This is repo contains a Nextflow pipline which performs continuum imaging and spectral imaging of stokes I, Q, U, V using [yandasoft](https://yandasoft.readthedocs.io/en/latest/index.html). This has been created to process calibrated measurement sets from CASDA for experiments.

Refer to the [yandasoft](https://yandasoft.readthedocs.io/en/latest/index.html) documentation for information about the processes used.

## Usage

You will need to have yandasoft installed locally or have Docker or Singularity. The yandasoft image can be found in [this url](https://hub.docker.com/r/csirocass/yandasoft) at Dockerhub

```
nextflow run askap-nextflow --in <measurement-set>

nextflow run askap-nextflow --in <measurement-set> -with-docker <docker-image>

# Example

nextflow askap-nextflow.nf --in scienceData.RACS_0012+00.SB45305.RACS_0012+00.beam10_averaged_cal.leakage.ms.tar

```

## TODO

1. Cache steps in the pipeline to avoid rerunning steps
2. Making the MPI parameters configurable
3. Auto detect direction value from measurement set
4. Use the yandasoft container
