#! /usr/bin/env nextflow

nextflow.enable.dsl=2

params.baseDir = "./"
params.in = "$params.baseDir/scienceData.RACS_0012+00.SB45305.RACS_0012+00.beam10_averaged_cal.leakage.ms.tar " // path to the measurement set file
params.out = "$params.baseDir/out" // output path

params.performExtractMsTar = true
params.performContinuumImaging = true
params.performSpectralImaging = true
params.imageSpectra = ["I", "Q", "U", "V"]
params.performSourceDetection = false
// params.direction_ra = "00h10m18.828"
// params.direction_dec = "+00.31.29.93"

params.meta = {}


process extract_ms_tar {
    publishDir "$params.out"

    input:
        path ms_tar_path

    output:
        path "*.ms"

    script:
    if (params.performExtractMsTar)
        """
        tar -xvf $ms_tar_path
        """
    else
        """
        echo "Extracting tar skipped"
        """
}

process continuum_imaging {
    publishDir "$params.out"

    input:
        path cont_imager_parset_ch
        path ms_file
        val image_name
        // val direction_ra
        // val direction_dec

    output:
        path "cont_imager.log"
        path "cont_imager.in"
        path "*.fits"

    when:
        params.performContinuumImaging

    script:
        """
        m4 -DMS_FILE_NAME=$ms_file -DFITS_NAME=$image_name ${cont_imager_parset_ch} > cont_imager.in
        mpiexec -n 4 imager -c cont_imager.in > cont_imager.log
        """
}

process spectral_imaging_I {
    // cpus 4
    publishDir "$params.out"

    input:
        path cont_cube_imager_I_parset_ch
        path ms_file
        val image_name
        // val direction_ra
        // val direction_dec
        val dummy_file // for dependency

    output:
        path "contcube_I_imager.log"
        path "contcube_imager_I.in"
        path "*.txt"
        path "*.fits"

    when:
        params.performSpectralImaging && params.imageSpectra.contains("I")

    script:
        """
        m4 -DMS_FILE_NAME=$ms_file -DFITS_NAME=$image_name ${cont_cube_imager_I_parset_ch} > contcube_imager_I.in
        mpiexec -n 4 imager -c contcube_imager_I.in > contcube_I_imager.log
        """
}

process spectral_imaging_Q {
    // cpus 4
    publishDir "$params.out"

    input:
        path cont_cube_imager_Q_parset_ch
        path ms_file
        val image_name
        // val direction_ra
        // val direction_dec
        val dummy_file // for dependency

    output:
        path "contcube_Q_imager.log"
        path "contcube_imager_Q.in"
        path "*.txt"
        path "*.fits"

    when:
        params.performSpectralImaging && params.imageSpectra.contains("Q")

    script:
        """
        m4 -DMS_FILE_NAME=$ms_file -DFITS_NAME=$image_name ${cont_cube_imager_Q_parset_ch} > contcube_imager_Q.in
        mpiexec -n 4 imager -c contcube_imager_Q.in > contcube_Q_imager.log
        """
}

process spectral_imaging_U {
    // cpus 4
    publishDir "$params.out"

    input:
        path cont_cube_imager_U_parset_ch
        path ms_file
        val image_name
        // val direction_ra
        // val direction_dec
        val dummy_file // for dependency

    output:
        path "contcube_U_imager.log"
        path "contcube_imager_U.in"
        path "*.txt"
        path "*.fits"

    when:
        params.performSpectralImaging && params.imageSpectra.contains("U")

    script:
        """
        m4 -DMS_FILE_NAME=$ms_file -DFITS_NAME=$image_name ${cont_cube_imager_U_parset_ch} > contcube_imager_U.in
        mpiexec -n 4 imager -c contcube_imager_U.in > contcube_U_imager.log
        """
}

process spectral_imaging_V {
    // cpus 4
    publishDir "$params.out"

    input:
        path cont_cube_imager_V_parset_ch
        path ms_file
        val image_name
        // val direction_ra
        // val direction_dec
        val dummy_file // for dependency

    output:
        path "contcube_V_imager.log"
        path "contcube_imager_V.in"
        path "*.txt"
        path "*.fits"

    when:
        params.performSpectralImaging && params.imageSpectra.contains("V")

    script:
        """
        m4 -DMS_FILE_NAME=$ms_file -DFITS_NAME=$image_name ${cont_cube_imager_V_parset_ch} > contcube_imager_V.in
        mpiexec -n 4 imager -c contcube_imager_V.in > contcube_V_imager.log
        """
}

process selavy {
  when:
    params.performSourceDetection

  // cpus 4
  publishDir "$params.out/selavy"

  input:
    path selavyIn_ch

  output:
    path "selavy.log"
    path "*.fits"

  script:
    """
    mpiexec -n 4 selavy -c ${selavyIn_ch} > selavy.log
    """
}

workflow {
    ms_tar_name = params.in
    ms_name = ms_tar_name.replace('.tar', '')

    // TODO: This logic has to be generic
    file_parts = ms_name.split('\\.')
    image_name = file_parts[-4] + "_" + file_parts[-3]
    println(file_parts)
    println(image_name)

    cont_imager_parset_ch = Channel.fromPath("$projectDir/parsets/cont_imager_template.in")
    contcubeImager_I_Path_ch = Channel.fromPath("$projectDir/parsets/contcube_imager_I_template.in")
    contcubeImager_Q_Path_ch = Channel.fromPath("$projectDir/parsets/contcube_imager_Q_template.in")
    contcubeImager_U_Path_ch = Channel.fromPath("$projectDir/parsets/contcube_imager_U_template.in")
    contcubeImager_V_Path_ch = Channel.fromPath("$projectDir/parsets/contcube_imager_V_template.in")

    extract_ms_tar(ms_tar_name)

    continuum_imaging(cont_imager_parset_ch, extract_ms_tar.out[0], image_name)

    // TODO: Figure out a better way to make the pipeline sequential

    spectral_imaging_I(contcubeImager_I_Path_ch, extract_ms_tar.out[0], image_name, continuum_imaging.out[0])
    spectral_imaging_Q(contcubeImager_Q_Path_ch, extract_ms_tar.out[0], image_name, spectral_imaging_I.out[0])
    spectral_imaging_U(contcubeImager_U_Path_ch, extract_ms_tar.out[0], image_name, spectral_imaging_Q.out[0])
    spectral_imaging_V(contcubeImager_V_Path_ch, extract_ms_tar.out[0], image_name, spectral_imaging_U.out[0])

}
